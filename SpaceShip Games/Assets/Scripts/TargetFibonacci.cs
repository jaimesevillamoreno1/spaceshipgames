using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetFibonacci : MonoBehaviour
{
    public int fibonacciNumber;
    public FibonacciController controller;
    public void ShootTarget()
    {
        controller.AddNumber(fibonacciNumber);
    }
}
