using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LogroUI : MonoBehaviour
{
    public TextMeshProUGUI texto;
    public GameObject logroUI;

    private void Awake()
    {
        logroUI.SetActive(false);
    }

    public void MostrarLogro(LogroType logro)
    {
        if(PlayerPrefs.HasKey(logro.ToString()))
        {
            return;
        }
        else
        {
            PlayerPrefs.SetString(logro.ToString(), logro.ToString());
        }

        switch(logro)
        {
            case LogroType.FibonacciPuzzle:
                texto.text = "Logro desbloqueado: Puzzle de fibonacci completado";
                StartCoroutine(LogroCoroutine());
                break;
            case LogroType.NewtonPuzzle:
                texto.text = "Logro desbloqueado: Puzzle de newton completado";
                StartCoroutine(LogroCoroutine());
                break;
            case LogroType.Parkour:
                texto.text = "Logro desbloqueado: Parkour completado";
                StartCoroutine(LogroCoroutine());
                break;
        }
    }

    public IEnumerator LogroCoroutine()
    {
        logroUI.SetActive(true);
        yield return new WaitForSeconds(2.5f);

        logroUI.SetActive(false);
    }
}
