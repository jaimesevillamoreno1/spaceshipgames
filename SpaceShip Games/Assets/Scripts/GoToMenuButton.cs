using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GoToMenuButton : MonoBehaviour
{
    PuzzleLifes puzzleLifes;
    Button button;

    private void Awake()
    {
        puzzleLifes = FindObjectOfType<PuzzleLifes>(true);
        button = GetComponent<Button>();
    }

    private void OnEnable()
    {
        button.onClick.AddListener(IrAlMenu);
    }

    private void OnDisable()
    {
        button.onClick.RemoveListener(IrAlMenu);
    }

    public void IrAlMenu()
    {

        int vidas = PlayerPrefs.GetInt("puzzleLifes");
        vidas--;
        PlayerPrefs.SetInt("puzzleLifes", vidas);
        Debug.LogWarning(vidas);
        if (vidas > 0)
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
        else
        {
            SceneManager.LoadScene("MenuScene");
        }
    }
}
