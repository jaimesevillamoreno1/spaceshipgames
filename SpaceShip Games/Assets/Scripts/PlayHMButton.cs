using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayHMButton : MonoBehaviour
{
    Button button;

    private void Awake()
    {
        button = GetComponent<Button>();
    }

    private void OnEnable()
    {
        button.onClick.AddListener(JugarHM);
    }

    private void OnDisable()
    {
        button.onClick.RemoveListener(JugarHM);
    }

    public void JugarHM()
    {
        GameManager.manager.hardMode = true;
        SceneManager.LoadScene("Nivel0");
    }
}
