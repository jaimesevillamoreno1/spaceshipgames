using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuManager : MonoBehaviour
{
    public void LoadMenuScene()
    {
        SceneLoader.Instance.LoadMainMenu();
        SceneLoader.Instance.RemoveGameScene();
    }
}
