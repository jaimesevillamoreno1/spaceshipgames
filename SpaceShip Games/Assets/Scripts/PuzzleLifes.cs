using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PuzzleLifes : MonoBehaviour
{
    public Image[] corazones;
    public int vidasRestantes = 3;

    private void Start()
    {
        if (!PlayerPrefs.HasKey("puzzleLifes"))
        {
            return;
        }
        vidasRestantes = PlayerPrefs.GetInt("puzzleLifes");
        if(vidasRestantes == 0)
        {
            PlayerPrefs.SetInt("puzzleLifes", 3);
            SceneManager.LoadScene("Nivel0");
        }
        else
        {
            ActualizarCorazones();
        }
    }

    public void ActualizarCorazones()
    {
        for (int i = 0; i < corazones.Length; i++)
        {
            if(i <= vidasRestantes - 1)
            {
                corazones[i].enabled = true;
            }
            else
            {
                corazones[i].enabled = false;
            }
        }
    }
}
