using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CogerFusil : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerController>().CogerFusil();
            Destroy(gameObject);
        }
    }
}
