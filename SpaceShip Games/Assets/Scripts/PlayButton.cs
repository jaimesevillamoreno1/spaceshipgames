using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayButton : MonoBehaviour
{
    Button button;

    private void Awake()
    {
        button = GetComponent<Button>();
    }

    private void OnEnable()
    {
        button.onClick.AddListener(Jugar);
    }

    private void OnDisable()
    {
        button.onClick.RemoveListener(Jugar);
    }

    public void Jugar()
    {
        GameManager.manager.hardMode = false;
        SceneManager.LoadScene("Nivel0");
    }
}
