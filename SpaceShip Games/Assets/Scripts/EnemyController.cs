using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    [SerializeField] ParticleSystem particulasAlMorir;
    [SerializeField] Transform puntoPatrulla1;
    [SerializeField] Transform puntoPatrulla2;
    NavMeshAgent agente;
    int destinoActual;
    [SerializeField] float radioDeteccion = 5f;
    [SerializeField] float radioDanoExplosion = 2f;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip sonidoMuerte;
    Transform jugador;

    private void Awake()
    {
        agente = GetComponent<NavMeshAgent>();
        jugador = FindObjectOfType<PlayerController>().transform;
    }

    private void Start()
    {
        agente.SetDestination(puntoPatrulla1.position);
        destinoActual = 1;
    }

    //Si alguien lo mata, muere
    public void Matar()
    {
        ParticleSystem particulas = Instantiate(particulasAlMorir, transform.position, Quaternion.identity);
        Collider[] colliders = Physics.OverlapSphere(transform.position, radioDanoExplosion);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].tag == "Player")
            {
                colliders[i].gameObject.GetComponent<PlayerController>().Matar();
            }
        }

        AudioSource audio = Instantiate(audioSource, transform.position, Quaternion.identity);
        audio.clip = sonidoMuerte;
        audio.Play();

        FindObjectOfType<LogroUI>(true).MostrarLogro(LogroType.PrimerEnemigoMuerto);
        Destroy(gameObject);
    }

    private void Update()
    {
        if (Vector3.Distance(jugador.position, transform.position) < 1f)
        {
            //Explotar si estamos cerca
            Matar();
            return;
        }

        Collider[] colliders = Physics.OverlapSphere(transform.position, radioDeteccion);

        for (int i = 0; i < colliders.Length; i++)
        {
            //Perseguir jugador si lo encuentra
            if(colliders[i].tag == "Player")
            {
                agente.SetDestination(colliders[i].transform.position);
            }
        }

        //Patrullar
        if(agente.remainingDistance < 0.1f)
        {
            if(destinoActual == 1)
            {
                agente.SetDestination(puntoPatrulla2.position);
                destinoActual = 2;
            }
            else if(destinoActual == 2)
            {
                agente.SetDestination(puntoPatrulla1.position);
                destinoActual = 1;
            }
        }
    }
}
