using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PortalToLevel1 : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            IrANivel1();
        }
    }

    public void IrANivel1()
    {
        SceneManager.LoadScene("Nivel1");
    }
}
