using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apple : MonoBehaviour
{
    public string color = "Verde";
    public AppleController appleController;

    public void DispararManzana(string tipoArma)
    {
        if(color == "Verde" && tipoArma == "Pistola")
        {
            appleController.QuitarManzana();
            Destroy(this.gameObject);
        }
        else if (color == "Roja" && tipoArma == "Fusil")
        {
            appleController.QuitarManzana();
            Destroy(this.gameObject);
        }
        else
        {
            appleController.Reiniciar();
        }
    }
}
