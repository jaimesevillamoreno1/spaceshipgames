using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    private void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;
    }
    public void LoadingGameScene_button()
    {
        SceneLoader.Instance.LoadGameScene();
        SceneLoader.Instance.RemoveMainMenu();
    }
   
}
