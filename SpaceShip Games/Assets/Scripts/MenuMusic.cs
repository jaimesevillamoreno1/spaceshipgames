using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MenuMusic : MonoBehaviour
{
    [SerializeField] AudioSource musicaMenu;
    // Start is called before the first frame update
    void Start()
    {
        musicaMenu = GetComponent<AudioSource>();
    }

    public void reproducirCancion()
    {
        musicaMenu.Play();
    }
}
