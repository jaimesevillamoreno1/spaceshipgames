using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CogerPistola : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerController>().CogerPistola();
            Destroy(gameObject);
        }
    }
}
