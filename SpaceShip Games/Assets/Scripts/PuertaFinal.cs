using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaFinal : MonoBehaviour
{
    [SerializeField] GameObject puerta;
    int placasPulsadas = 0;

    public void PulsarPlaca()
    {
        placasPulsadas++;
        if(placasPulsadas == 2)
        {
            puerta.SetActive(false);
        }
    }
}
