using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoTorreta : MonoBehaviour
{
    public GameObject objetoAMover;
    public Transform puntoInicio;
    public Transform puntoFinal;

    public float velocidadMovimiento;

    private Vector3 moverHacia;
    // Start is called before the first frame update
    void Start()
    {
        moverHacia = puntoFinal.position;
    }

    // Update is called once per frame
    void Update()
    {
        objetoAMover.transform.position = Vector3.MoveTowards(objetoAMover.transform.position, moverHacia, velocidadMovimiento * Time.deltaTime);

        if(objetoAMover.transform.position == puntoFinal.position)
        {
            moverHacia = puntoInicio.position;
        }
        if (objetoAMover.transform.position == puntoInicio.position)
        {
            moverHacia = puntoFinal.position;
        }
    }
}
