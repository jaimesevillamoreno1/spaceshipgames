using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogroParkour : MonoBehaviour
{
    public bool firstTime = true;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && firstTime)
        {
            firstTime = false;
            FindObjectOfType<LogroUI>(true).MostrarLogro(LogroType.Parkour);
        }
    }
}
