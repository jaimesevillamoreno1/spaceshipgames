using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class zonaMuerteInteludio2 : MonoBehaviour
{
    public PuzzleLifes puzzleLifes;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            PlayerPrefs.SetInt("puzzleLifes", puzzleLifes.vidasRestantes - 1);
            SceneManager.LoadScene("Interludio2");
        }
    }
}
