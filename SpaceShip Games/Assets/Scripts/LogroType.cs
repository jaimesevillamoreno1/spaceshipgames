using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LogroType
{
    FibonacciPuzzle = 0,
    NewtonPuzzle = 1,
    Parkour = 2,
    PrimerEnemigoMuerto = 3,
}
