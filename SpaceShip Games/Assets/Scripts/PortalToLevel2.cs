using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PortalToLevel2 : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            IrANivel2();
        }
    }

    public void IrANivel2()
    {
        SceneManager.LoadScene("Nivel2");
    }
}
