using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetearLogros : MonoBehaviour
{
    private void Awake()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.SetInt("puzzleLifes", 3);
    }
}
