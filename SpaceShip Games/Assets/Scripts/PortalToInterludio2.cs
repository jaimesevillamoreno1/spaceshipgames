using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PortalToInterludio2 : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            IrAInterludio2();
        }
    }

    public void IrAInterludio2()
    {
        SceneManager.LoadScene("Interludio2");
    }
}
