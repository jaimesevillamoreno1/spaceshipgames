using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class GameManager : MonoBehaviour
{
    public static GameManager manager;
    public bool hardMode;
    float endTimer;
    float deathTimer = 180;
    public GameObject actualPlayer;
    public TextMeshProUGUI textoContaMuerte;
    bool empezar = false;

    private void Awake()
    {
        if (manager == null)
            manager = this;

        DontDestroyOnLoad(this.gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        if (hardMode)
        {

            if (empezar)
            {
                textoContaMuerte.enabled = true;
                endTimer -= Time.deltaTime;
                textoContaMuerte.text = endTimer.ToString();

                if (endTimer <= 0)
                {
                    actualPlayer.GetComponent<PlayerController>().Matar();
                    StopTimer();
                }
            }
        }
        else if(textoContaMuerte = null)
            textoContaMuerte.enabled = false;
    }

    public void StartTimer()
    {
        endTimer = deathTimer;
        empezar = true;
    }
    public void StopTimer()
    {
        endTimer = deathTimer;
        empezar = true;
    }
}
