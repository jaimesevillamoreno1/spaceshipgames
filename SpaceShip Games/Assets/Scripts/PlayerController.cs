using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerController : MonoBehaviour
{
    //Variable con la velocidad de movimiento
    [SerializeField] float velocidad = 10f;
    private float guardarVel;
    [SerializeField] float velocidadCarrera = 15f;
    [SerializeField] float velocidadCamara = 10f;
    [SerializeField] float velocidadSalto = 10f;
    CharacterController controller;
    float v, h, rot;
    Vector3 vectorMovimiento;
    float velocidadVertical;
    [SerializeField] GameObject pistola;
    [SerializeField] GameObject fusil;
    int balasPistola = 0;
    int balasFusil = 0;
    [SerializeField] AudioClip pistolaSonido;
    [SerializeField] AudioClip fusilSonido;
    AudioSource audioSource;
    [SerializeField] Transform camara;
    [SerializeField] GameObject panelMuerteUI;
    public bool hasMuerto = false;
    [SerializeField] AudioClip sonidoMuerte;
    Animator animator;
    bool enMovimeinto = false;
    public GameObject emisorNiebla;

    public TextMeshProUGUI municionpistola;
    public TextMeshProUGUI municionfusil;
    public TextMeshProUGUI tContaMuerte;

    //Indicar que eje queremos y multiplicarlo por la velocidad y el time delta time


    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        audioSource = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
        pistola.SetActive(false);
        fusil.SetActive(false);

        
        audioSource.Play();
        GameManager.manager.actualPlayer = this.gameObject;
        if (GameManager.manager.hardMode)
        {
            GameManager.manager.StartTimer();
            GameManager.manager.textoContaMuerte = tContaMuerte;
            tContaMuerte.enabled = true;
            emisorNiebla.SetActive(true);
            
        }

        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        

        municionpistola.text = "Munici�n pistola: " + balasPistola;
        municionfusil.text = "Munici�n fusil: " + balasFusil;


        if (hasMuerto)
        {
            Cursor.lockState = CursorLockMode.None;
            return;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
        }


        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");

        guardarVel = velocidad;

        if ((h > 0.1f || h < -0.1f || v > 0.1f || v < -0.1f) && !enMovimeinto)
        {
            velocidad = guardarVel;
            animator.SetBool("isRunning", true);
        }
        else
        {
            animator.SetBool("isRunning", false);
        }

        vectorMovimiento = transform.right * h + transform.forward * v;

        if (Input.GetButtonDown("Jump") && controller.isGrounded)
        {
            velocidadVertical = velocidadSalto;
            animator.SetTrigger("Jump");
        }



        velocidadVertical -= 11f * Time.deltaTime;
        vectorMovimiento.y = velocidadVertical;
        controller.Move(vectorMovimiento * velocidad * Time.deltaTime);
        if (Input.GetKey(KeyCode.LeftShift) && enMovimeinto)
        {
            controller.Move(vectorMovimiento * velocidadCarrera * Time.deltaTime);
        }

        rot += Input.GetAxis("Mouse X") * velocidadCamara;
        transform.localEulerAngles = transform.TransformDirection(new Vector3(0, rot, 0));

        if (Input.GetMouseButtonDown(0) && balasPistola > 0 && pistola.activeSelf)
        {
            StartCoroutine(shootpistoldelay());
        }
        if (Input.GetMouseButtonDown(1) && balasFusil > 0 && fusil.activeSelf)
        {
            StartCoroutine(shootfusilDelay());
        }

    }

    IEnumerator shootpistoldelay()
    {
        yield return new WaitForSeconds(0.1f);
        //Disparar pistola
        balasPistola--;
        audioSource.clip = pistolaSonido;
        audioSource.Play();
        yield return new WaitForSeconds(0.2f);
        DispararRayo("Pistola");
        animator.SetTrigger("ShootLeft");

    }

    IEnumerator shootfusilDelay()
    {
        yield return new WaitForSeconds(0.1f);
        //Disparar fusil
        balasFusil--;
        audioSource.clip = fusilSonido;
        audioSource.Play();
        DispararRayo("Fusil");
        animator.SetTrigger("ShootRight");


    }
    public void DispararRayo(string tipoArma)
    {
        RaycastHit hit;
        //Crear rayo hacia delante y ver si choca con alg�n enemigo
        if (Physics.Raycast(camara.position, camara.forward, out hit))
        {
            if (hit.transform.tag == "Enemigo")
            {
                hit.transform.gameObject.GetComponent<EnemyController>().Matar();
            }
            else if (hit.transform.tag == "Barril")
            {
                hit.transform.gameObject.GetComponent<BarrelController>().Explotar();
            }
            else if(hit.transform.tag == "Diana")
            {
                hit.transform.gameObject.GetComponent<TargetFibonacci>().ShootTarget();
            }
            else if(hit.transform.tag == "Apple")
            {
                hit.transform.gameObject.GetComponent<Apple>().DispararManzana(tipoArma);
            }
        }
    }

    public void CogerBalasPistola()
    {
        if(balasPistola == 0)
        {
            balasPistola += 5;
        }
    }

    public void CogerBalasFusil()
    {
        if(balasFusil == 0)
        {
            balasFusil += 5;
        }
    }

    public void RecargarPistola()
    {
        //Si recarga poner un tope de 5 balas

        if (Input.GetKeyDown(KeyCode.E) && balasPistola == 0)
        {
            balasPistola += 5;
        }
    }

    public void RecargarFusil()
    {
        //Si recarga poner un tope de 5 balas

        if (Input.GetKeyDown(KeyCode.R) && balasFusil == 0)
        {
            balasFusil += 5;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "BalasPistola" && balasPistola == 0)
        {
            Destroy(other.gameObject);
            CogerBalasPistola();
        }

        if (other.tag == "BalasFusil" && balasFusil == 0)
        {
            Destroy(other.gameObject);
            CogerBalasFusil();
        }
    }

    //Para cuando mueres
    public void Matar()
    {
        hasMuerto = true;
        audioSource.clip = sonidoMuerte;
        audioSource.Play();
        panelMuerteUI.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
    }

    public void CogerPistola()
    {
        pistola.SetActive(true);
    }

    public void CogerFusil()
    {
        fusil.SetActive(true);
    }
}
