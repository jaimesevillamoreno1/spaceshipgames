using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelController : MonoBehaviour
{
    [SerializeField] ParticleSystem particulasAlExplotar;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip sonidoExplosion;

    public void Explotar()
    {
        ParticleSystem particulas = Instantiate(particulasAlExplotar, transform.position, Quaternion.identity);
        Collider[] colliders = Physics.OverlapSphere(transform.position, 2f);
        for (int i = 0; i < colliders.Length; i++)
        {
            if(colliders[i].tag == "Player")
            {
                colliders[i].gameObject.GetComponent<PlayerController>().Matar();
            }
        }

        AudioSource audio = Instantiate(audioSource, transform.position, Quaternion.identity);
        audio.clip = sonidoExplosion;
        audio.Play();

        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            Explotar();
        }
    }
}
