using UnityEngine;

public class PressurePlate : MonoBehaviour
{
    bool seHaActivado = false;
    AudioSource audioSource;
    [SerializeField] AudioClip audio;
    Light light;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        light = GetComponent<Light>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(seHaActivado)
        {
            return;
        }

        if(other.tag == "Player")
        {
            seHaActivado = true;
            audioSource.clip = audio;
            audioSource.Play();
            FindObjectOfType<PuertaFinal>().PulsarPlaca();
            light.color = Color.yellow;
        }
    }
}
