using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    AudioSource audioSource;
    [SerializeField] AudioClip audio;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            audioSource.clip = audio;
            audioSource.Play();
            other.gameObject.GetComponent<PlayerController>().Matar();
        }
    }
}
