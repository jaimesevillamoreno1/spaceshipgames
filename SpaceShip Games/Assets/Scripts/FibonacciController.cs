using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FibonacciController : MonoBehaviour
{
    public int[] sequence = { 1, 1, 2, 3, 5 };
    private int currentIndex = 0;
    public GameObject puerta;
    public AudioSource audioSource;
    public AudioClip correcto;
    public AudioClip incorrecto;

    //Comprobar nuevo numero secuencia
    public void AddNumber(int number)
    {
        if(currentIndex >= 5)
        {
            return;
        }

        if(sequence[currentIndex] == number)
        {
            Debug.Log("N�mero correcto");
            audioSource.clip = correcto;
            audioSource.Play();
            if(currentIndex <= 4)
            {
                if(currentIndex == 4)
                {
                    AbrirPuerta();
                }
                currentIndex++;
            }
            else
            {
            }
        }
        else
        {
            Debug.Log("N�mero erroneo");
            audioSource.clip = incorrecto;
            audioSource.Play();
            currentIndex = 0;
        }
    }

    void AbrirPuerta()
    {

        FindObjectOfType<LogroUI>(true).MostrarLogro(LogroType.FibonacciPuzzle);
        puerta.SetActive(false);
    }
}
