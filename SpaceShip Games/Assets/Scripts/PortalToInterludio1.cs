using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PortalToInterludio1 : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            IrAInterludio1();
        }
    }

    public void IrAInterludio1()
    {
        SceneManager.LoadScene("Interludio1");
    }
}
