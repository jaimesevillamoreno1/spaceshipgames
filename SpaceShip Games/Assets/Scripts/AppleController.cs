using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AppleController : MonoBehaviour
{
    public PuzzleLifes puzzleLifes;
    public int applesNumber = 5;
    public GameObject puerta;

    public void QuitarManzana()
    {
        applesNumber--;
        if(applesNumber == 0)
        {
            AbrirPuerta();
        }
    }

    public void Reiniciar()
    {
        PlayerPrefs.SetInt("puzzleLifes", puzzleLifes.vidasRestantes - 1);
        SceneManager.LoadScene("Interludio1");
    }

    public void AbrirPuerta()
    {
        FindObjectOfType<LogroUI>(true).MostrarLogro(LogroType.NewtonPuzzle);
        puerta.SetActive(false);
    }
}
